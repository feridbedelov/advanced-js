Hamburger.SIZE_SMALL = { size: "SIZE_SMALL", type: "size", cal: 20, price: 50 };
Hamburger.SIZE_LARGE = {
  size: "SIZE_LARGE",
  type: "size",
  cal: 40,
  price: 100
};
Hamburger.STUFFING_CHEESE = {
  name: "STUFFING_CHEESE",
  type: "stuffing",
  cal: 20,
  price: 10
};
Hamburger.STUFFING_SALAD = {
  name: "STUFFING_SALAD",
  type: "stuffing",
  cal: 5,
  price: 20
};
Hamburger.STUFFING_POTATO = {
  name: "STUFFING_POTATO",
  type: "stuffing",
  cal: 10,
  price: 15
};
Hamburger.TOPPING_MAYO = {
  name: "TOPPING_MAYO",
  type: "topping",
  cal: 15,
  price: 20
};
Hamburger.TOPPING_SPICE = {
  name: "TOPPING_SPICE",
  type: "topping",
  cal: 0,
  price: 15
};

function HamburgerExeption(message) {
  this.message = message;
  this.name = "HamburgerExeption";
}

function Hamburger(size, stuffing) {
  try {
    if (size.type !== "size") {
      throw new HamburgerExeption("Size is not valid");
    }
    if (stuffing.type !== "stuffing") {
      throw new HamburgerExeption("Stuffing is not valid");
    }
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
  } catch (error) {
    console.log(error);
  }
}

Hamburger.prototype.addTopping = function(topping) {
  try {
    if (!topping || topping.type !== "topping") {
      throw new HamburgerExeption("Topping is not valid");
    }

    if (!this.toppings.includes(topping)) {
      this.toppings.push(topping);
    } else {
      throw new HamburgerExeption("This topping is already added!");
    }
  } catch (error) {
    console.log(error);
  }
};

Hamburger.prototype.removeTopping = function(topping) {
  try {
    if (!topping || topping.type !== "topping") {
      throw new HamburgerExeption("Topping is not valid");
    }

    if (this.toppings.includes(topping)) {
      this.toppings.splice(
        this.toppings.findIndex(v => v.name === topping.name),
        1
      );
    } else {
      throw new HamburgerExeption("This topping is not in the list!");
    }
  } catch (error) {
    console.log(error);
  }
};

Hamburger.prototype.getToppings = function() {
  return this.toppings;
};

Hamburger.prototype.getSize = function() {
  return this.size.size;
};

Hamburger.prototype.getStuffing = function() {
  return this.stuffing.name;
};

Hamburger.prototype.calculatePrice = function() {
  var resultPrice = this.size.price + this.stuffing.price;
  for (let i = 0; i < this.toppings.length; i++) {
    resultPrice += this.toppings[i].price;
  }
  return resultPrice;
};

Hamburger.prototype.calculateCalories = function() {
  var resultCal = this.size.cal + this.stuffing.cal;
  for (let index = 0; index < this.toppings.length; index++) {
    resultCal += this.toppings[index].cal;
  }
  return resultCal;
};

var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);

console.log("Price : " + hamburger.calculatePrice());
console.log("Calories : " + hamburger.calculateCalories());
console.log("Size : " + hamburger.getSize());
console.log("Stuffing : " + hamburger.getStuffing());
console.log("Toppings : " + hamburger.getToppings());
