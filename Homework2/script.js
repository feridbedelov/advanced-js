class Hamburger {
  constructor(size, stuffing) {
    try {
      if (size.type !== "size") {
        throw new HamburgerExeption("Size is not valid");
      }
      if (stuffing.type !== "stuffing") {
        throw new HamburgerExeption("Stuffing is not valid");
      }
      this.size = size;
      this.stuffing = stuffing;
      this.toppings = [];
    } catch (error) {
      console.log(error);
    }
  }

  set addTopping(topping) {
    try {
      if (!topping || topping.type !== "topping") {
        throw new HamburgerExeption("Topping is not valid");
      }

      if (!this.toppings.includes(topping)) {
        this.toppings.push(topping);
      } else {
        throw new HamburgerExeption("This topping is already added!");
      }
    } catch (error) {
      console.log(error);
    }
  }

  removeTopping(topping) {
    try {
      if (!topping || topping.type !== "topping") {
        throw new HamburgerExeption("Topping is not valid");
      }

      if (this.toppings.includes(topping)) {
        this.toppings.splice(
          this.toppings.findIndex(v => v.name === topping.name),
          1
        );
      } else {
        throw new HamburgerExeption("This topping is not in the list!");
      }
    } catch (error) {
      console.log(error);
    }
  }

  get Size() {
    return this.size.size;
  }

  get Stuffing() {
    return this.stuffing.name;
  }

  get Toppings() {
    return this.toppings;
  }

  calculatePrice() {
    let price = this.size.price + this.stuffing.price;
    for (let i = 0; i < this.toppings.length; i++)
      price += this.toppings[i].price;
    return price;
  }

  calculateCalories() {
    let calories = this.size.cal + this.stuffing.cal;
    for (let i = 0; i < this.toppings.length; i++)
      calories += this.toppings[i].cal;
    return calories;
  }
}

Hamburger.SIZE_SMALL = { size: "SIZE_SMALL", type: "size", cal: 20, price: 50 };
Hamburger.SIZE_LARGE = {
  size: "SIZE_LARGE",
  type: "size",
  cal: 40,
  price: 100
};
Hamburger.STUFFING_CHEESE = {
  name: "STUFFING_CHEESE",
  type: "stuffing",
  cal: 20,
  price: 10
};
Hamburger.STUFFING_SALAD = {
  name: "STUFFING_SALAD",
  type: "stuffing",
  cal: 5,
  price: 20
};
Hamburger.STUFFING_POTATO = {
  name: "STUFFING_POTATO",
  type: "stuffing",
  cal: 10,
  price: 15
};
Hamburger.TOPPING_MAYO = {
  name: "TOPPING_MAYO",
  type: "topping",
  cal: 15,
  price: 20
};
Hamburger.TOPPING_SPICE = {
  name: "TOPPING_SPICE",
  type: "topping",
  cal: 0,
  price: 15
};

class HamburgerExeption {
  constructor(message) {
    this.message = message;
    this.name = "HamburgerExeption";
  }
}

let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
hamburger.addTopping = Hamburger.TOPPING_MAYO;
hamburger.addTopping = Hamburger.TOPPING_SPICE;
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping = Hamburger.TOPPING_MAYO;

console.log("Price : " + hamburger.calculatePrice());
console.log("Calories : " + hamburger.calculateCalories());
console.log("Size : " + hamburger.Size);
console.log("Stuffing : " + hamburger.Stuffing);
console.log("Toppings : " + hamburger.Toppings);
